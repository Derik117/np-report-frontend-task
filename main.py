import uvicorn
from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
app = FastAPI()
from data import data
templates = Jinja2Templates('')
@app.get('/')
def report(request: Request):
    return templates.TemplateResponse('report.html', {'request': request, 'data': data})



if __name__ == '__main__':
    uvicorn.run('main:app', port=8000, reload=True)